# todoist-helpers

[![pipeline status](https://img.shields.io/endpoint?url=https://hydra.ist.nicht-so.sexy/job/todoist-helpers/master/package.x86_64-linux/shield)](https://hydra.ist.nicht-so.sexy/job/todoist-helpers/master/package.x86_64-linux/)

A few helper scripts for my Todoist.
