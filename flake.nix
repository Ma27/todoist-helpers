{
  description = "A few helper scripts for my Todoist";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: {
    overlay = final: prev: {
      todoist-helpers = final.rustPlatform.buildRustPackage {
        pname = "todoist-helpers";
        version = "0.0.1-dev";
        src = final.lib.cleanSource ./.;
        nativeBuildInputs = [ final.pkg-config ];
        buildInputs = [ final.openssl ];
        cargoLock.lockFile = ./Cargo.lock;
        RUST_TEST_THREADS = 1;
        dontUseCargoParallelTests = true;
      };
    };

    hydraJobs.package.x86_64-linux = self.defaultPackage.x86_64-linux;
  } // flake-utils.lib.eachSystem [ "x86_64-linux" ] (system: let
    pkgs = import nixpkgs {
      inherit system;
      overlays = [ self.overlay ];
    };
  in {
    packages = { inherit (pkgs) todoist-helpers; };
    defaultPackage = pkgs.todoist-helpers;

    devShell = with pkgs; mkShell {
      name = "devshell";
      RUST_TEST_THREADS = 1;
      buildInputs = [
        cargo rustc rustfmt rustPackages.clippy pkg-config openssl
      ];
    };
  });
}
