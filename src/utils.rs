use crate::{data, output};
use indoc::indoc;
use termion::{color, style};

pub(crate) fn show_task(task: &data::Task, project: &data::Project) -> String {
    format!(
        indoc! {"
            • {}{} ({}{}{}){}
              From project: {}{}{}
        "},
        output::ansi_for_tty(style::Bold, libc::STDOUT_FILENO as i32),
        task.content,
        output::ansi_color_out!(color::Blue),
        task.url(),
        output::ansi_color_out!(color::White),
        output::ansi_for_tty(style::Reset, libc::STDOUT_FILENO as i32),
        output::ansi_for_tty(style::Bold, libc::STDOUT_FILENO as i32),
        project.name,
        output::ansi_for_tty(style::Reset, libc::STDOUT_FILENO as i32),
    )
}

pub(crate) fn display_reminder(p: &data::Project, r: &data::Reminder, t: &data::Task) -> String {
    format!(
        indoc! {"
            {}
              Reminder due: {}{}{}{}{}
        "},
        {
            let mut x = show_task(t, p);
            x.truncate(x.len() - 1);
            x
        },
        output::ansi_for_tty(style::Bold, libc::STDOUT_FILENO as i32),
        output::ansi_color_out!(color::Green),
        r.due.date.format("%Y-%m-%d"),
        output::ansi_color_out!(color::White),
        output::ansi_for_tty(style::Reset, libc::STDOUT_FILENO as i32),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{offset::Local, DateTime, FixedOffset, NaiveDate};

    #[test]
    fn test_show_task() {
        let raw = show_task(
            &data::Task {
                id: 23,
                due: None,
                content: "Snens".to_string(),
                project_id: 23,
                labels: vec![],
                parent_id: None,
            },
            &data::Project {
                id: 23,
                name: "Foobar".to_string(),
            },
        );
        let out = raw.lines().collect::<Vec<_>>();

        assert!(out.get(0).unwrap().contains("Snens"));
        assert!(out
            .get(0)
            .unwrap()
            .contains("https://todoist.com/app/#task%2F23"));
        assert!(out.get(1).unwrap().contains("Foobar"));
        assert_eq!(2, out.len());
    }

    #[test]
    fn test_display_reminder() {
        let t = data::Task {
            id: 23,
            due: None,
            content: "Snens".to_string(),
            project_id: 23,
            labels: vec![],
            parent_id: None,
        };
        let p = data::Project {
            id: 23,
            name: "Foobar".to_string(),
        };
        let r = data::Reminder {
            item_id: 23,
            id: 23,
            due: data::Due {
                is_recurring: false,
                date: DateTime::<Local>::from_utc(
                    NaiveDate::from_ymd(2023, 5, 23).and_hms(0, 0, 0),
                    FixedOffset::east(0),
                ),
            },
        };

        let r1 = show_task(&t, &p);
        let r2 = display_reminder(&p, &r, &t);

        assert_eq!(
            r1[0..(r1.len() - 1)],
            r2.lines().take(2).collect::<Vec<_>>().join("\n")
        );
        assert!(r2
            .lines()
            .skip(2)
            .collect::<Vec<_>>()
            .join("\n")
            .contains("2023-05-23"));
    }
}
