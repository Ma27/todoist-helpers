use chrono::{offset::Local, DateTime};
use serde::{Deserialize, Serialize};

use std::cmp::Ordering;

macro_rules! todoist_entity {
    ($a:ident, ($(#[$mp:meta])? $primary:ident: $prim_type:ty), {
        $($(#[$m:meta])? pub $field_name:ident: $field_type:ty,)*
    }) => {
        #[derive(Debug, Clone, Eq, Serialize, Deserialize)]
        pub struct $a {
            $(#[$mp])?
            pub $primary: $prim_type,
            $(
                $(#[$m])?
                pub $field_name: $field_type,
            )*
        }

        impl PartialEq for $a {
            fn eq(&self, other: &Self) -> bool {
                self.$primary == other.$primary
            }
        }
    }
}

todoist_entity! {
    Project,
    (#[serde(with = "api_id")] id: u64),
    { pub name: String, }
}

todoist_entity! {
    Reminder,
    (#[serde(with = "api_id")] id: u64),
    {
        #[serde(with = "api_id")]
        pub item_id: u64,
        pub due: Due,
    }
}

todoist_entity! {
    Stats,
    (days_items: Vec<DayStat>),
    {}
}

todoist_entity! {
    Task,
    (#[serde(with = "api_id")] id: u64),
    {
        #[serde(with = "api_id")]
        pub project_id: u64,
        pub content: String,
        pub labels: Vec<String>,
        pub due: Option<Due>,
        pub parent_id: Option<String>, // FIXME switch back to u64 here as well
    }
}

impl Task {
    pub fn url(&self) -> String {
        format!("https://todoist.com/app/#task%2F{}", self.id)
    }
}

todoist_entity! {
    Label,
    (#[serde(with = "api_id")] id: u64),
    { pub name: String, }
}

impl Ord for Reminder {
    fn cmp(&self, other: &Self) -> Ordering {
        self.due.date.cmp(&other.due.date)
    }
}
impl PartialOrd for Reminder {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.due.date.cmp(&other.due.date))
    }
}

#[derive(Debug, Clone, Eq, Serialize, Deserialize)]
pub struct Due {
    pub is_recurring: bool,
    #[serde(with = "api_date")]
    pub date: DateTime<Local>,
}
impl PartialEq for Due {
    fn eq(&self, other: &Self) -> bool {
        self.date == other.date
    }
}

#[derive(Debug, Clone, Eq, Serialize, Deserialize)]
pub struct DayStat {
    #[serde(with = "api_date")]
    pub date: DateTime<Local>,
    pub total_completed: u8,
}
impl PartialEq for DayStat {
    fn eq(&self, other: &Self) -> bool {
        self.date == other.date
    }
}

mod api_id {
    use serde::{self, Deserialize, Deserializer, Serializer};

    pub fn serialize<S>(input: &u64, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer
    {
        let s = input.to_string();
        serializer.serialize_str(&s)
    }
    pub fn deserialize<'de, D>(deserializer: D) -> Result<u64, D::Error>
    where
        D: Deserializer<'de>
    {
        let s = String::deserialize(deserializer)?;
        s.parse::<u64>().map_err(serde::de::Error::custom)
    }
}

mod api_date {
    use chrono::{offset::Local, DateTime, LocalResult, NaiveDate, NaiveDateTime, TimeZone};
    use serde::{self, Deserialize, Deserializer, Serializer};

    const FORMAT: &'static str = "%Y-%m-%d";
    const FORMAT_REM: &'static str = "%Y-%m-%dT%H:%M:%S";

    pub fn serialize<S>(date: &DateTime<Local>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", date.format(FORMAT));
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Local>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;

        // `datetime` is only provided if a reminder is given, otherwise
        // it's only YYYY-MM-DD, so we have to take care of both cases
        // and assume that the timezone of the user is equal to the timezone where
        // the user runs this program.
        //
        // See also https://developer.todoist.com/rest/v1/#tasks
        match NaiveDate::parse_from_str(&s, FORMAT)
            .map_err(|_| ())
            .and_then(|x| match Local.from_local_date(&x) {
                LocalResult::Single(x) => Ok(x.and_hms(0, 0, 0)),
                _ => Err(()),
            }) {
            Err(_) => NaiveDateTime::parse_from_str(&s, FORMAT_REM)
                .map_err(serde::de::Error::custom)
                .and_then(|x| match Local.from_local_datetime(&x) {
                    LocalResult::Single(x) => Ok(x.date().and_hms(0, 0, 0)),
                    _ => Err(String::from("invalid date")).map_err(serde::de::Error::custom),
                }),
            Ok(x) => Ok(x),
        }
    }
}

impl Ord for Task {
    fn cmp(&self, other: &Self) -> Ordering {
        compare_by_due(&self.due, &other.due)
    }
}
impl PartialOrd for Task {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(compare_by_due(&self.due, &other.due))
    }
}

fn compare_by_due(d1: &Option<Due>, d2: &Option<Due>) -> Ordering {
    match d1 {
        Some(d1_) => match d2 {
            Some(n) => d1_.date.cmp(&n.date),
            None => Ordering::Less,
        },
        None => match d2 {
            Some(_) => Ordering::Greater,
            None => Ordering::Equal,
        },
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_order_tasks() {
        use chrono::prelude::*;
        let mut t = vec![
            Task {
                id: 23,
                project_id: 5,
                content: String::from("Foo"),
                labels: vec![],
                due: None,
                parent_id: None,
            },
            Task {
                id: 24,
                project_id: 5,
                content: String::from("Bar"),
                labels: vec![],
                due: Some(Due {
                    is_recurring: false,
                    date: DateTime::<Local>::from_utc(
                        NaiveDate::from_ymd(2023, 5, 23).and_hms(0, 0, 0),
                        FixedOffset::east(0),
                    ),
                }),
                parent_id: None,
            },
            Task {
                id: 25,
                project_id: 5,
                content: String::from("Baz"),
                labels: vec![],
                due: Some(Due {
                    is_recurring: false,
                    date: DateTime::<Local>::from_utc(
                        NaiveDate::from_ymd(2024, 5, 23).and_hms(0, 0, 0),
                        FixedOffset::east(0),
                    ),
                }),
                parent_id: None,
            },
        ];

        t.sort();

        assert_eq!(24, t.get(0).unwrap().id);
        assert_eq!(25, t.get(1).unwrap().id);
        assert_eq!(23, t.get(2).unwrap().id);
    }
}
