use reqwest;
use std::env;

use log::error;

#[cfg(test)]
use mockito;

#[derive(Debug, PartialEq)]
pub enum ApiError {
    MissingToken,
    Unknown,
    WrongToken,
    NotFound,
    Serialization,
}

pub enum API {
    #[allow(dead_code)]
    Rest,
    Sync,
}

impl API {
    pub fn prefix(&self) -> &str {
        match self {
            API::Rest => "rest/v1",
            API::Sync => "sync/v9",
        }
    }
}

pub async fn todoist_request(
    route: &str,
    api_type: API,
    form: Option<Vec<(String, String)>>,
) -> Result<String, ApiError> {
    let token = env::var("TODOIST_TOKEN").or(Err(ApiError::MissingToken))?;

    #[cfg(not(test))]
    let url = "https://api.todoist.com";
    #[cfg(test)]
    let url = &mockito::server_url();

    let mut builder = reqwest::Client::new()
        .get(format!("{}/{}{}", url, api_type.prefix(), route))
        .header(reqwest::header::AUTHORIZATION, format!("Bearer {}", token));

    if let Some(form_val) = form {
        let mut form_ = form_val.clone();
        form_.push((String::from("token"), token));
        builder = builder.query(&form_);
    }
    let response = builder.send().await.or(Err(ApiError::Unknown))?;

    if !response.status().is_success() {
        error!("Failed to submit a request to {}: {:?}", url, response);
    }

    match response.status() {
        reqwest::StatusCode::UNAUTHORIZED | reqwest::StatusCode::FORBIDDEN => {
            Err(ApiError::WrongToken)
        }
        reqwest::StatusCode::NOT_FOUND | reqwest::StatusCode::GONE => Err(ApiError::NotFound),
        reqwest::StatusCode::OK => Ok(response.text().await.or(Err(ApiError::Unknown))?),
        _ => Err(ApiError::Unknown),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use mockito::mock;

    #[tokio::test(flavor = "multi_thread")]
    async fn test_missing_token() {
        std::env::remove_var("TODOIST_TOKEN");
        assert_error(
            todoist_request("/", API::Rest, None).await,
            ApiError::MissingToken,
        );
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_wrong_route() {
        std::env::set_var("TODOIST_TOKEN", "demo");
        let _m = mock("GET", "/rest/v1/invalid/resource")
            .with_status(404)
            .create();
        assert_error(
            todoist_request("/invalid/resource", API::Rest, None).await,
            ApiError::NotFound,
        );
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_unauthorized() {
        std::env::set_var("TODOIST_TOKEN", "demo");
        let _m = mock("GET", "/rest/v1/projects").with_status(401).create();
        assert_error(
            todoist_request("/projects", API::Rest, None).await,
            ApiError::WrongToken,
        );
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_response() {
        std::env::set_var("TODOIST_TOKEN", "demo");
        let _m = mock("GET", "/rest/v1/projects")
            .with_status(200)
            .with_body("{}")
            .create();
        assert_eq!(
            "{}",
            todoist_request("/projects", API::Rest, None).await.unwrap()
        );
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_query() {
        std::env::set_var("TODOIST_TOKEN", "demo");
        let _m = mock("GET", "/rest/v1/projects?foo=bar&token=demo")
            .with_status(200)
            .with_body("{}")
            .create();
        let r = todoist_request(
            "/projects",
            API::Rest,
            Some(vec![(String::from("foo"), String::from("bar"))]),
        )
        .await
        .unwrap();

        assert_eq!("{}", r);
    }

    fn assert_error<T>(n: Result<T, ApiError>, expected: ApiError) {
        match n {
            Ok(_) => panic!("Expected error result!"),
            Err(x) => assert_eq!(x, expected),
        }
    }
}
