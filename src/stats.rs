use crate::cache;

use std::io;
use std::sync::mpsc; 
use std::thread;
use std::time::Duration;
use termion::{
    event::Key,
    input::{MouseTerminal, TermRead},
    raw::IntoRawMode,
    screen::AlternateScreen
};

use tui::{
    Terminal,
    backend::TermionBackend,
    layout::{Constraint, Direction, Layout},
    widgets::{Block, Borders},
};

pub fn stats_browse() -> Result<String, cache::CacheError> {
    let stdout = AlternateScreen::from(MouseTerminal::from(io::stdout().into_raw_mode().unwrap()));
    let mut term = Terminal::new(TermionBackend::new(stdout)).unwrap();

    let (tx, rx) = mpsc::channel(); 
    thread::spawn(move || {
        let stdin = io::stdin();
        for e in stdin.keys() {
            if let Ok(x) = e {
                tx.send(Some(x)).unwrap();
            }
        }
    }); 

    loop {
        term.draw(|f| {
            let chunks = Layout::default()
                .constraints([Constraint::Length(3), Constraint::Min(0)].as_ref())
                .split(f.size());
            let block = Block::default().title("Block").borders(Borders::ALL);
            f.render_widget(block, chunks[0]);
        }).unwrap();

        let d = Duration::from_millis(10);
        let r = rx.recv_timeout(d).unwrap_or(None); 
        match r {
            Some(Key::Char('q')) => break,
            _ => {},
        }
    }

    Ok(String::from("success"))
}
