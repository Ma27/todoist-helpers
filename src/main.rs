mod cache;
mod data;
mod output;
mod reqwest;
mod utils;

use crate::output::ERR;

use data::*;
use utils::*;

use clap::*;

use std::time::Duration;

use termion::{color, style};
use tokio::time;

use chrono::DateTime;
use chrono_english::{parse_date_string, Dialect};

use log::LevelFilter;

#[derive(Debug, Clone)]
struct DateTimeWrapper {
    data: DateTime<chrono::Local>,
    original: String,
}

use std::str::FromStr;
impl FromStr for DateTimeWrapper {
    type Err = String;
    fn from_str(item: &str) -> Result<Self, Self::Err> {
        let today = cache::today();
        match parse_date_string(item, today, Dialect::Us) {
            Err(_) => Err(String::from(format!(
                "cannot interpret given date `{}'!",
                item
            ))),
            Ok(data) => Ok(DateTimeWrapper {
                data,
                original: String::from(item),
            }),
        }
    }
}

#[derive(Parser)]
#[command(version = "0.1", author = "Maximilian Bosch")]
struct Opts {
    /// Whether to sync against Todoist's API
    #[arg(short, long = "sync")]
    sync: bool,

    /// If specified, the sync is aborted after the specified amount in seconds.
    #[arg(long = "sync-timeout")]
    sync_timeout: Option<u8>,

    #[command(subcommand)]
    subcmd: SubCommand,
}

#[derive(Parser)]
enum SubCommand {
    /// Display a short summary of scheduled vs. resolved tasks
    Stats,

    /// Show reminders for tasks in Todoist
    Reminders(Reminders),

    /// Display items that have neither a Due date nor a reminder
    UnscheduledItems(UnscheduledItems),
}

#[derive(Args, Clone)]
struct UnscheduledItems {
    #[arg(short, long = "ignore-scheduled-children")]
    ignore_scheduled_children: bool,
}

#[derive(Args, Clone)]
struct Reminders {
    #[arg()]
    when: DateTimeWrapper,

    #[arg(short, long = "all")]
    all: bool,
}

#[tokio::main]
async fn main() {
    let formatter = syslog::Formatter3164 {
        facility: syslog::Facility::LOG_USER,
        hostname: None,
        process: String::from("todoist-helpers"),
        pid: std::process::id() as i32,
    };
    let logger = syslog::unix(formatter).expect("Cannot connect to syslog!");
    log::set_boxed_logger(Box::new(syslog::BasicLogger::new(logger)))
        .map(|()| log::set_max_level(LevelFilter::Info))
        .expect("Cannot initialize logger!");

    let opts: Opts = Opts::parse();

    handle_result(match &opts.subcmd {
        SubCommand::Stats => stats(opts).await,
        SubCommand::Reminders(r) => reminders(r.when.clone(), r.all, opts).await,
        SubCommand::UnscheduledItems(u) => unscheduled(u.ignore_scheduled_children, opts).await,
    });
}

fn is_scheduled(item: &data::Task, cache: &cache::Cache) -> bool {
    !(item.due.is_none()
        && cache
            .reminders
            .iter()
            .find(|reminder| reminder.item_id == item.id)
            .is_none())
}

async fn unscheduled(
    ignore_scheduled_children: bool,
    opts: Opts,
) -> Result<String, cache::CacheError> {
    let cache = open_cache(opts).await?;

    let mut filtered = cache
        .items
        .iter()
        .filter(|item| {
            let scheduled = is_scheduled(item, &cache);
            // If item itself is scheduled we're fine
            !scheduled
                && !(
                    // With `-i` specified, items are omitted if a child-item is scheduled
                    // already.
                    ignore_scheduled_children
                        && cache
                            .items
                            .iter()
                            .find(|item_| {
                                item_.parent_id.clone().map_or_else(|| 0, |x| x.parse::<u64>().expect("Expected parent id to be an u64")) == item.id
                                    && is_scheduled(item_, &cache)
                            })
                            .is_some()
                )
        })
        .collect::<Vec<_>>();

    sort_by_project(&mut filtered);
    let result = filtered
        .iter()
        .map(|x| show_task(x, cache.projects.get(&x.project_id).unwrap()))
        .collect::<Vec<_>>();

    if result.len() == 0 {
        Ok(format!(
            "{}No unscheduled items{}",
            output::ansi_color_out!(color::Yellow),
            output::ansi_for_tty(style::Reset, libc::STDOUT_FILENO as i32)
        ))
    } else {
        Ok(format!("{}", result.join("\n")))
    }
}

fn sort_by_project(items: &mut Vec<&Task>) {
    items.sort_by_key(|x| x.project_id);
}

async fn reminders(
    when: DateTimeWrapper,
    all: bool,
    opts: Opts,
) -> Result<String, cache::CacheError> {
    let cache = open_cache(opts).await?;
    let compare = when.data.date();

    let mut filtered = cache
        .reminders
        .iter()
        .skip_while(|n| !all && (n.due.date.date() < compare))
        .take_while(|n| n.due.date.date() <= compare)
        .map(|r| (r, cache.items.iter().find(|&x| x.id == r.item_id).unwrap()))
        .collect::<Vec<_>>();

    filtered.sort_by_key(|(_, t)| t.project_id);

    let relevant = filtered
        .iter()
        .map(|(r, t)| display_reminder(cache.projects.get(&t.project_id).unwrap(), r, t))
        .collect::<Vec<_>>();

    if relevant.len() == 0 {
        Ok(format!(
            "{}No reminders for {}{} ({}){}{}!",
            output::ansi_color_out!(color::Yellow),
            output::ansi_for_tty(style::Bold, libc::STDOUT_FILENO as i32),
            when.original,
            compare,
            output::ansi_for_tty(style::Reset, libc::STDOUT_FILENO as i32),
            output::ansi_color_out!(color::Yellow),
        ))
    } else {
        Ok(format!("{}", relevant.join("\n")))
    }
}

fn handle_result(x: Result<String, cache::CacheError>) {
    match x {
        Ok(x) => eprintln!("{}", x),
        Err(x) => {
            match x {
                cache::CacheError::CorruptedFile => eprintln!(
                    "{}The cache-file ({}{}{}{}) seems to be corrupted!{}",
                    output::ansi_color_err!(color::Red),
                    output::ansi_for_tty(style::Bold, libc::STDERR_FILENO as i32),
                    cache::Cache::cache_file().unwrap().to_str().unwrap(),
                    output::ansi_for_tty(style::Reset, libc::STDERR_FILENO as i32),
                    output::ansi_color_err!(color::Red),
                    output::ansi_for_tty(style::Reset, libc::STDERR_FILENO as i32),
                ),
                cache::CacheError::ReadError => eprintln!(
                    "{}Cannot read from the cache-file. Does it exist and has correct permissions?{}",
                    output::ansi_color_err!(color::Red),
                    output::ansi_for_tty(style::Reset, libc::STDERR_FILENO as i32),
                ),
                cache::CacheError::SyncError(x) => handle_sync_error(Err(x)),
            };

            std::process::exit(1)
        }
    }
}

fn handle_sync_error(x: Result<(), reqwest::ApiError>) {
    match x {
        Ok(_) => (),
        Err(x) => {
            match x {
                reqwest::ApiError::MissingToken => eprintln!(
                    "{}Please set your Todoist API token using the env-var {}TODOIST_API_TOKEN{}{}!{}",
                    output::ansi_color_err!(color::Red),
                    output::ansi_for_tty(style::Bold, libc::STDERR_FILENO as i32),
                    output::ansi_for_tty(style::Reset, libc::STDERR_FILENO as i32),
                    output::ansi_color_err!(color::Red),
                    output::ansi_for_tty(style::Reset, libc::STDERR_FILENO as i32),
                ),
                reqwest::ApiError::NotFound => eprintln!(
                    "{}Internal error: resource not found!{}",
                    output::ansi_color_err!(color::Red),
                    output::ansi_for_tty(style::Reset, libc::STDERR_FILENO as i32),
                ),
                reqwest::ApiError::Serialization => eprintln!(
                    "{}JSON decode error! The API may have changed and an update of the package is needed.{}",
                    output::ansi_color_err!(color::Red),
                    output::ansi_for_tty(style::Reset, libc::STDERR_FILENO as i32),
                ),
                reqwest::ApiError::WrongToken => eprintln!(
                    "{}API request rejected. Are you sure your token in {}TODOIST_API_TOKEN{}{} is correct?{}",
                    output::ansi_color_err!(color::Red),
                    output::ansi_for_tty(style::Bold, libc::STDERR_FILENO as i32),
                    output::ansi_for_tty(style::Reset, libc::STDERR_FILENO as i32),
                    output::ansi_color_err!(color::Red),
                    output::ansi_for_tty(style::Reset, libc::STDERR_FILENO as i32),
                ),
                reqwest::ApiError::Unknown => eprintln!(
                    "{}Unknown error while calling Todoist API!{}",
                    output::ansi_color_err!(color::Red),
                    output::ansi_for_tty(style::Reset, libc::STDERR_FILENO as i32),
                ),
            };
            std::process::exit(1)
        }
    }
}

async fn stats(opts: Opts) -> Result<String, cache::CacheError> {
    let cache = open_cache(opts).await?;
    let today = cache::today();

    let planned_today = cache
        .items
        .iter()
        .take_while(|&b| b.due.as_ref().map(|d| d.date <= today).unwrap_or(false))
        .collect::<Vec<_>>()
        .len();

    let completed_today = cache.stat.days_items.get(0).unwrap().total_completed;

    Ok(format!(
        " {}/{}",
        completed_today,
        completed_today + (planned_today as u8)
    ))
}

async fn open_cache(opts: Opts) -> Result<cache::Cache, cache::CacheError> {
    if opts.sync {
        match opts.sync_timeout {
            None => do_sync(false).await,
            Some(wait_s) => {
                let cache_rs =
                    time::timeout(Duration::from_secs(wait_s as u64), do_sync(true)).await;
                if let Ok(cache) = cache_rs {
                    cache
                } else {
                    cache::Cache::from_cache_file()
                }
            }
        }
    } else {
        cache::Cache::from_cache_file()
    }
}

async fn do_sync(allow_failure: bool) -> Result<cache::Cache, cache::CacheError> {
    let mut cache = cache::Cache::from_cache_file()?;
    let sync_fut = cache.sync();
    if allow_failure {
        sync_fut.await.unwrap_or(());
    } else {
        sync_fut.await?;
    }
    Ok(cache)
}

#[cfg(test)]
mod tests {
    use super::*;
    use cache::prepare_test;
    use regex::Regex;

    #[tokio::test(flavor = "multi_thread")]
    async fn test_stats() {
        let _tmpfile = prepare_test("stats.json".to_string());
        assert!((stats(Opts {
            sync: false,
            subcmd: SubCommand::Stats,
            sync_timeout: None,
        })
        .await)
            .unwrap()
            .ends_with("10/12"));
    }

    #[test]
    fn test_parse_date_str() {
        let d1 = DateTimeWrapper::from_str("foo");
        let d2 = DateTimeWrapper::from_str("today");
        let d3 = DateTimeWrapper::from_str("next monday");

        assert!(!d1.is_ok());
        assert!(d2.is_ok());
        assert!(d3.is_ok());
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_get_reminders_for() {
        let _tmpfile = prepare_test("reminders.json".to_string());

        let now = cache::today();
        let w = DateTimeWrapper {
            original: String::from("today"),
            data: now,
        };
        let output = {
            let r = reminders(
                w.clone(),
                false,
                Opts {
                    sync: false,
                    subcmd: SubCommand::Reminders(Reminders {
                        when: w.clone(),
                        all: false,
                    }),
                    sync_timeout: None,
                },
            )
            .await;
            r.unwrap()
        };
        assert!(output.contains("Laundry"));
        assert!(!output.contains("Gardening"));
        assert!(output.contains("Dishes"));
        assert!(output.contains("Shopping"));
        // dirty, but ensure the correct order
        let re = Regex::new(r"(?s:.*)Laundry(?s:.*)Dishes(?s:.*)Shopping(?s:.*)").unwrap();
        assert!(re.is_match(&output));
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_unscheduled_items() {
        let _tmpfile = prepare_test("unscheduled-items-cache.json".to_string());
        let output = {
            let r = unscheduled(
                false,
                Opts {
                    sync: false,
                    subcmd: SubCommand::UnscheduledItems(UnscheduledItems {
                        ignore_scheduled_children: false,
                    }),
                    sync_timeout: None,
                },
            )
            .await;
            r.unwrap()
        };
        assert!(!output.contains("Laundry"));
        assert!(!output.contains("Gardening"));
        assert!(output.contains("Sleep"));
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_unscheduled_items_ignore_with_scheduled_children() {
        let _tmpfile = prepare_test("unscheduled-items-ignore-cache.json".to_string());
        let output = {
            let r = unscheduled(
                true,
                Opts {
                    sync: false,
                    subcmd: SubCommand::UnscheduledItems(UnscheduledItems {
                        ignore_scheduled_children: true,
                    }),
                    sync_timeout: None,
                },
            )
            .await;
            r.unwrap()
        };
        assert!(output.contains("Laundry"));
        // reminder
        assert!(!output.contains("Gardening"));
        // child with reminder
        assert!(!output.contains("Sleep"));
        // due dates
        assert!(!output.contains("Parenting"));
    }
}
