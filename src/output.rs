use lazy_static::lazy_static;
use libc;
use std::fmt::Display;

lazy_static! {
    pub static ref OUT: i32 = libc::STDOUT_FILENO as i32;
    pub static ref ERR: i32 = libc::STDERR_FILENO as i32;
}

macro_rules! ansi_color {
    ($v:expr, $w:expr) => {
        output::ansi_for_tty(color::Fg($v), $w)
    };
}
macro_rules! ansi_color_out {
    ($v:expr) => {
        output::ansi_color!($v, *output::OUT)
    };
}
macro_rules! ansi_color_err {
    ($v:expr) => {
        output::ansi_color!($v, *ERR)
    };
}

pub(crate) use ansi_color;
pub(crate) use ansi_color_err;
pub(crate) use ansi_color_out;

pub(crate) fn ansi_for_tty(code: impl Display, fd: i32) -> String {
    let istty = unsafe { libc::isatty(fd) } != 0;
    if istty {
        format!("{}", code)
    } else {
        String::from("")
    }
}
