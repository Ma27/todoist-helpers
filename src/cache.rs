use crate::data::*;
use crate::reqwest::*;
use chrono::prelude::*;
use dirs;
use futures::executor::block_on;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::write;
use std::path::PathBuf;

use log::info;

use serde_json;

#[cfg(test)]
pub(crate) fn prepare_test(filename: String) -> tempfile::TempDir {
    std::env::set_var("TODOIST_TOKEN", "demo");
    let d = tempfile::tempdir().unwrap();

    std::env::set_var("XDG_CACHE_HOME", d.path().to_str().unwrap());

    let file = format!(
        "{}/todoist-helpers-cache-v{}.json",
        d.path().to_str().unwrap(),
        *VERSION
    );

    let mut dir = std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    dir.push("resources/test");
    dir.push(filename);
    std::fs::write(
        file.clone(),
        std::fs::read_to_string(dir)
            .unwrap()
            .replace("@file@", &file)
            .to_string(),
    )
    .unwrap();

    d
}

macro_rules! sort_copy {
    ($x:ident, $y:ident, $z: ident) => {
        let mut i = $z.$x;
        i.sort_unstable();
        $y.$x = i;
    };
}

#[derive(Debug, PartialEq)]
pub enum CacheError {
    CorruptedFile,
    ReadError,
    SyncError(ApiError),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Cache {
    pub labels: HashMap<u64, Label>,
    pub projects: HashMap<u64, Project>,
    pub items: Vec<Task>,
    pub stat: Stats,
    pub reminders: Vec<Reminder>,
    cache_file: PathBuf,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct CacheIntermediate {
    labels: Vec<Label>,
    projects: Vec<Project>,
    items: Vec<Task>,
    reminders: Vec<Reminder>,
}

lazy_static! {
    pub static ref VERSION: u8 = 6;
}

impl Cache {
    pub fn from_cache_file() -> Result<Cache, CacheError> {
        let cache_target = Self::cache_file()?;
        if !cache_target.exists() {
            let mut new = Cache {
                labels: HashMap::new(),
                projects: HashMap::new(),
                items: vec![],
                stat: Stats { days_items: vec![] },
                reminders: vec![],
                cache_file: cache_target,
            };
            block_on(new.sync())?;
            Ok(new)
        } else {
            let content = std::fs::read_to_string(cache_target).or(Err(CacheError::ReadError))?;
            let data: Cache = serde_json::from_str(&content).or(Err(CacheError::CorruptedFile))?;

            Ok(data)
        }
    }

    pub fn cache_file() -> Result<PathBuf, CacheError> {
        let mut cache_target = dirs::cache_dir().ok_or(CacheError::ReadError)?;
        cache_target.push(format!("todoist-helpers-cache-v{}.json", *VERSION));

        Ok(cache_target)
    }

    pub async fn sync(&mut self) -> Result<(), CacheError> {
        info!("Syncing cache");
        let headers = vec![
            ("sync_token", "*"),
            (
                "resource_types",
                "[\"projects\",\"labels\",\"items\",\"reminders\"]",
            ),
        ];
        let sync_resp = todoist_request(
            "/sync",
            API::Sync,
            Some(
                headers
                    .into_iter()
                    .map(|(x, y)| (String::from(x), String::from(y)))
                    .collect::<Vec<_>>(),
            ),
        )
        .await
        .map_err(|t| CacheError::SyncError(t))?;

        let raw: CacheIntermediate = serde_json::from_str(&sync_resp)
            .unwrap();//.or(Err(CacheError::SyncError(ApiError::Serialization)))?;

        for proj in raw.projects {
            self.projects.insert(proj.id, proj);
        }

        sort_copy!(items, self, raw);
        sort_copy!(reminders, self, raw);

        for label in raw.labels {
            self.labels.insert(label.id, label);
        }

        self.stat = serde_json::from_str(
            &todoist_request("/completed/get_stats", API::Sync, Some(vec![]))
                .await
                .map_err(|t| CacheError::SyncError(t))?,
        )
        .unwrap();

        if let Some(todays_stats) = self.stat.days_items.get(0) {
            let today = today();

            // it is save to assume that the stats are ordered descending by date.
            if today > todays_stats.date {
                let mut fixup = vec![DayStat {
                    date: today,
                    total_completed: 0,
                }];

                fixup.append(&mut self.stat.days_items);

                self.stat.days_items = fixup;
            }
        }

        serde_json::to_string(&self)
            .or(Err(CacheError::CorruptedFile))
            .and_then(|c| {
                write(self.cache_file.to_str().unwrap(), c).or(Err(CacheError::CorruptedFile))
            })
    }
}

#[cfg(not(test))]
pub fn today() -> DateTime<Local> {
    Local::today().and_hms(0, 0, 0)
}

#[cfg(test)]
pub fn today() -> DateTime<Local> {
    Local.timestamp(1, 0)
}

#[cfg(test)]
mod tests {
    use super::*;
    use mockito::mock;
    use serde_json::json;
    use std::env;

    #[test]
    fn test_existing_cache() {
        let _tmpfile = prepare_test("cache-unit-existing.json".to_string());
        let cache = Cache::from_cache_file().unwrap();

        assert_eq!(0, cache.items.len());
        assert_eq!("Foo", cache.projects.get(&23).unwrap().name);

        assert_eq!(42, cache.stat.days_items.get(0).unwrap().total_completed);
    }

    #[test]
    fn test_corrupted() {
        let _tmpfile = prepare_test("cache-unit-corrupted.json".to_string());

        let result = Cache::from_cache_file();

        assert!(!result.is_ok());
        match result {
            Ok(_) => panic!("expected error!"),
            Err(x) => assert_eq!(x, CacheError::CorruptedFile),
        }
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_load_from_api() {
        let data = json!({
            "reminders": [
                {
                    "id": "1",
                    "item_id": "23",
                    "due": {
                        "date": "2016-09-02",
                        "is_recurring": false,
                    },
                },
                {
                    "id": "2",
                    "item_id": "24",
                    "due": {
                        "date": "2015-09-02",
                        "is_recurring": false,
                    }
                }
            ],
            "projects": [
                {
                    "id": "42",
                    "name": "Chores"
                }
            ],
            "labels": [
                {
                    "id": "1337",
                    "name": "In progress"
                }
            ],
            "items": [
                {
                    "id": "23",
                    "project_id": "42",
                    "content": "Gardening",
                    "labels": ["In progress"],
                    "due": {
                        "date": "2016-09-02",
                        "datetime": "2016-09-01T09:00:00Z",
                        "is_recurring": false,
                        "string": "tomorrow at 12",
                        "timezone": "Europe/Moscow"
                    },
                },
                {
                    "id": "24",
                    "project_id": "42",
                    "content": "Laundry",
                    "labels": ["In progress"],
                    "due": {
                        "date": "2016-09-01",
                        "datetime": "2016-09-01T09:00:00Z",
                        "is_recurring": false,
                        "string": "tomorrow at 12",
                        "timezone": "Europe/Moscow"
                    },
                }
            ]
        });

        let stat = json!({
            "days_items": [
                {
                    "date": "1999-12-31",
                    "total_completed": 5,
                }
            ]
        });

        env::set_var("TODOIST_TOKEN", "demo");
        let _m = mock("GET", "/sync/v9/sync?sync_token=*&resource_types=%5B%22projects%22%2C%22labels%22%2C%22items%22%2C%22reminders%22%5D&token=demo")
            .with_status(200)
            .with_header("content-type", "application/json")
            .with_header("Connection", "close")
            .with_body(data.to_string())
            .create();

        let _m2 = mock("GET", "/sync/v9/completed/get_stats?token=demo")
            .with_status(200)
            .with_header("content-type", "application/json")
            .with_header("Connection", "close")
            .with_body(stat.to_string())
            .create();

        let d = tempfile::tempdir().unwrap();
        env::set_var("XDG_CACHE_HOME", d.path().to_str().unwrap());

        let cache = Cache::from_cache_file().unwrap();

        assert_eq!(
            format!(
                "{}/todoist-helpers-cache-v{}.json",
                d.path().to_str().unwrap(),
                *VERSION
            ),
            cache.cache_file.to_str().unwrap()
        );

        assert!(cache.labels.contains_key(&1337));
        assert_eq!(2, cache.items.len());

        assert_eq!("Gardening", cache.items.last().unwrap().content);

        assert_eq!(5, cache.stat.days_items.get(0).unwrap().total_completed);
        assert_eq!(2, cache.reminders.len());

        _m.assert();
        _m2.assert();
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_insert_empty_stats_on_12_am() {
        // This is to check for the following behavior:
        // I live in a different timezone in contrast to Todoist so on my 12AM the API doesn't
        // seem to provide new stats. So if the latest stats in the cache are smaller than today's date
        // an empty stat-item is inserted to avoid showing bogus data somewhere.
        let data = json!({
            "projects": [],
            "labels": [],
            "items": [],
            "reminders": [],
        });

        let stat = json!({
            "days_items": [
                {
                    "date": "1969-12-31",
                    "total_completed": 5,
                }
            ]
        });

        env::set_var("TODOIST_TOKEN", "demo");
        let _m = mock("GET", "/sync/v9/sync?sync_token=*&resource_types=%5B%22projects%22%2C%22labels%22%2C%22items%22%2C%22reminders%22%5D&token=demo")
            .with_status(200)
            .with_header("content-type", "application/json")
            .with_header("Connection", "close")
            .with_body(data.to_string())
            .create();

        let _m2 = mock("GET", "/sync/v9/completed/get_stats?token=demo")
            .with_status(200)
            .with_header("content-type", "application/json")
            .with_header("Connection", "close")
            .with_body(stat.to_string())
            .create();

        let d = tempfile::tempdir().unwrap();
        env::set_var("XDG_CACHE_HOME", d.path().to_str().unwrap());

        let cache = Cache::from_cache_file().unwrap();

        assert_eq!(2, cache.stat.days_items.len());
    }
}
